; Papt profiler make file
core = "7.x"
api = "2"
; projects[] = drupal
; comment this out in to use on drupal.org
projects[drupal][version] = "7.23"

; Modules
projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][subdir] = "contrib"

projects[addressfield][version] = "1.0-beta5"
projects[addressfield][subdir] = "contrib"

projects[addthis][version] = "4.0-alpha2"
projects[addthis][subdir] = "contrib"

projects[auto_nodetitle][version] = "1.0"
projects[auto_nodetitle][subdir] = "contrib"

projects[backup_migrate][version] = "2.8"
projects[backup_migrate][subdir] = "contrib"

projects[colorbox][version] = "2.5"
projects[colorbox][subdir] = "contrib"

projects[commerce][version] = "1.8"
projects[commerce][subdir] = "contrib"

projects[commerce_ccavenue][version] = "1.0"
projects[commerce_ccavenue][subdir] = "contrib"

projects[commerce_cod][version] = "1.0"
projects[commerce_cod][subdir] = "contrib"

projects[commerce_extra][version] = "1.0-alpha1"
projects[commerce_extra][subdir] = "contrib"

projects[commerce_extra_panes][version] = "1.1"
projects[commerce_extra_panes][subdir] = "contrib"

projects[commerce_flat_rate][version] = "1.0-beta2"
projects[commerce_flat_rate][subdir] = "contrib"

projects[commerce_shipping][version] = "2.0"
projects[commerce_shipping][subdir] = "contrib"

projects[commerce_stock][version] = "1.2"
projects[commerce_stock][subdir] = "contrib"

projects[computed_field][version] = "1.0"
projects[computed_field][subdir] = "contrib"

projects[conditional_fields][version] = "3.0-alpha1"
projects[conditional_fields][subdir] = "contrib"

projects[contact_permissions][version] = "1.x-dev"
projects[contact_permissions][subdir] = "contrib"

projects[context_admin][version] = "1.2"
projects[context_admin][subdir] = "contrib"

projects[ctools][version] = "1.3"
projects[ctools][subdir] = "contrib"

projects[date][version] = "2.6"
projects[date][subdir] = "contrib"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

projects[entity][version] = "1.2"
projects[entity][subdir] = "contrib"

projects[environment_indicator][version] = "1.1"
projects[environment_indicator][subdir] = "contrib"

projects[fblikebutton][version] = "2.3"
projects[fblikebutton][subdir] = "contrib"

projects[fboauth][version] = "1.6"
projects[fboauth][subdir] = "contrib"

projects[features][version] = "2.0"
projects[features][subdir] = "contrib"

projects[field_collection][version] = "1.0-beta5"
projects[field_collection][subdir] = "contrib"

projects[field_group][version] = "1.3"
projects[field_group][subdir] = "contrib"

projects[field_permissions][version] = "1.0-beta2"
projects[field_permissions][subdir] = "contrib"

projects[fivestar][version] = "2.0-alpha2"
projects[fivestar][subdir] = "contrib"

projects[flag][version] = "2.1"
projects[flag][subdir] = "contrib"

projects[globalredirect][version] = "1.5"
projects[globalredirect][subdir] = "contrib"

projects[gmap][version] = "2.7"
projects[gmap][subdir] = "contrib"

projects[google_analytics][version] = "1.4"
projects[google_analytics][subdir] = "contrib"

projects[inline_entity_form][version] = "1.3"
projects[inline_entity_form][subdir] = "contrib"

projects[jcaption][version] = "1.3"
projects[jcaption][subdir] = "contrib"

projects[jquery_update][version] = "2.3"
projects[jquery_update][subdir] = "contrib"

projects[job_scheduler][version] = "2.0-alpha3"
projects[job_scheduler][subdir] = "contrib"

projects[libraries][version] = "2.1"
projects[libraries][subdir] = "contrib"

projects[link][version] = "1.2"
projects[link][subdir] = "contrib"

projects[location][version] = "3.0-rc2"
projects[location][subdir] = "contrib"

projects[mailchimp][version] = "2.12"
projects[mailchimp][subdir] = "contrib"

projects[modal_forms][version] = "1.2"
projects[modal_forms][subdir] = "contrib"

projects[module_filter][version] = "1.8"
projects[module_filter][subdir] = "contrib"

projects[mollom][version] = "2.8"
projects[mollom][subdir] = "contrib"

projects[nodereference_url][version] = "1.12"
projects[nodereference_url][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[panels][version] = "3.3"
projects[panels][subdir] = "contrib"

projects[phone][version] = "2.x-dev"
projects[phone][subdir] = "contrib"

projects[publish_button][version] = "1.0-beta8"
projects[publish_button][subdir] = "contrib"

projects[references][version] = "2.1"
projects[references][subdir] = "contrib"

projects[robotstxt][version] = "1.1"
projects[robotstxt][subdir] = "contrib"

projects[rules][version] = "2.6"
projects[rules][subdir] = "contrib"

projects[simple_gmap][version] = "1.1"
projects[simple_gmap][subdir] = "contrib"

projects[site_map][version] = "1.0"
projects[site_map][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"

projects[views][version] = "3.7"
projects[views][subdir] = "contrib"

projects[video_embed_field][version] = "2.0-beta5"
projects[video_embed_field][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.1"
projects[views_bulk_operations][subdir] = "contrib"

projects[votingapi][version] = "2.11"
projects[votingapi][subdir] = "contrib"

projects[wysiwyg][version] = "2.2"
projects[wysiwyg][subdir] = "contrib"

; Themes
; skeletontheme
projects[skeletontheme][type] = "theme"
projects[skeletontheme][version] = "1.2"
projects[skeletontheme][subdir] = "contrib"
; bootstrap
projects[bootstrap][type] = "theme"
projects[bootstrap][version] = "2.0-beta3"
projects[bootstrap][subdir] = "contrib"

; Libraries
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"
libraries[colorbox][destination] = "libraries"
libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/1.x.zip"

libraries[libphonenumber-for-php][directory_name] = "libphonenumber-for-php"
libraries[libphonenumber-for-php][type] = "library"
libraries[libphonenumber-for-php][destination] = "libraries"
libraries[libphonenumber-for-php][download][type] = "git"
libraries[libphonenumber-for-php][download][url] = "https://github.com/chipperstudios/libphonenumber-for-php.git"

libraries[tinymce][directory_name] = "tinymce"
libraries[tinymce][type] = "library"
libraries[tinymce][destination] = "libraries"
libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "http://github.com/downloads/tinymce/tinymce/tinymce_3.5.8.zip"

libraries[bootstrap][directory_name] = "bootstrap"
libraries[bootstrap][type] = "library"
libraries[bootstrap][destination] = "themes/contrib/bootstrap/"
libraries[bootstrap][download][type] = "get"
libraries[bootstrap][download][url] = "https://github.com/twbs/bootstrap/archive/v3.0.3.zip"


